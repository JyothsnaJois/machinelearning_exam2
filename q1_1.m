clc;close all;clear all;
%% Generating datasets

mu(:,1)=[-2;0];mu(:,2)=[2;0];
c(:,:,1)=[1 -0.9;-0.9 2];
c(:,:,2)=[2 0.9;0.9 1];
P=[0.9,0.1];
n1=10;n2=100;n3=1000;n4=10000;
%% 10 samples
label1 = (rand(1,n1) >= P(1));
N1 = [length(find(label1==0)),length(find(label1==1))]; % number of samples from each class
x1 = zeros(2,n1); 
for l = 0:1
    x1(:,label1==l) = mvnrnd(mu(:,l+1),c(:,:,l+1),N1(l+1))';
end
n11=0;n12=0;
for i=1:n1
    if label1(i)==0
        n11=n11+1;
    else 
        n12=n12+1;
    end
end
%% 100 samples
label2 = (rand(1,n2) >= P(1));
N2 = [length(find(label2==0)),length(find(label2==1))]; % number of samples from each class
x2 = zeros(2,n2); 
for l = 0:1
    x2(:,label2==l) = mvnrnd(mu(:,l+1),c(:,:,l+1),N2(l+1))';
end
n21=0;n22=0;
for i=1:n2
    if label2(i)==0
        n21=n21+1;
    else 
        n22=n22+1;
    end
end
%% 1000 samples
label3 = (rand(1,n3) >= P(1));
N3 = [length(find(label3==0)),length(find(label3==1))]; % number of samples from each class
x3 = zeros(2,n3); 
for l = 0:1
    x3(:,label3==l) = mvnrnd(mu(:,l+1),c(:,:,l+1),N3(l+1))';
end
n31=0;n32=0;
for i=1:n3
    if label3(i)==0
        n31=n31+1;
    else 
        n32=n32+1;
    end
end
%% 10000 samples
label4 = rand(1,n4) >= P(1);
N4 = [length(find(label4==0)),length(find(label4==1))]; % number of samples from each class
x4 = zeros(2,n4); 
for l = 0:1
    x4(:,label4==l) = mvnrnd(mu(:,l+1),c(:,:,l+1),N4(l+1))';
end
n41=0;n42=0;
for i=1:n4
    if label4(i)==0
        n41=n41+1;
    else 
        n42=n42+1;
    end
end
%% liklihood test
lamda=[0 1;1 0];
gamma=log((lamda(1,2)-lamda(2,2))/lamda(2,1)-lamda(1,1));
discriminantScore4 = log(evalGaussian(x4,mu(:,2),c(:,:,2)))-log(evalGaussian(x4,mu(:,1),c(:,:,1)));
decision4 = discriminantScore4 >=gamma;

%% liklihood test for dataset 4
ind410 = find(decision4==1 & label4==0); % probability of false positive
ind411 = find(decision4==1 & label4==1); % probability of True positive
ind400 = find(decision4==0 & label4==0); % probability of true negative
ind401 = find(decision4==0 & label4==1); % probability of false negative
    p410 = length(ind410)/n41;
    p411 = length(ind411)/n42;
    p400 = length(ind400)/n41;
    p401 = length(ind401)/n42;
    p4r1  =([p410,p401]*N4')/n4;
    [A1,B1]=min(p4r1);
%% using classifier on 10k samples
gamma1=[-n4:n4];
for i=1:20001
    decision4 = discriminantScore4 >= gamma1(i);
    ind4410 = find(decision4==1 & label4==0); % probability of false positive
    ind4411 = find(decision4==1 & label4==1); % probability of True positive
    ind4400 = find(decision4==0 & label4==0); % probability of true negative
    ind4401 = find(decision4==0 & label4==1); % probability of false negative
    p4410(i) = length(ind4410)/n41;
    p4411(i) = length(ind4411)/n42;
    p4400(i) = length(ind4400)/n41;
    p4401(i) = length(ind4401)/n42;
    p44r1(i)  =([p4410(i),p4401(i)]*N4')/n4;
end
[A,B]=min(p44r1);
figure(1);plot(p4410,p4411);
hold on
plot(p4410(B),p4411(B),'r*');
hold on
plot(p410,p411,'b*');
hold off
title('ROC plot');xlabel('false positive');ylabel('true positive');
%% plot decision boundaries
figure(2), clf,
plot(x4(1,label4==0),x4(2,label4==0),'o'), hold on,p410
plot(x4(1,label4==1),x4(2,label4==1),'+'), axis equal,
legend('Class 0','Class 1'), 
title('Data and their true labels'),
xlabel('x_1'), ylabel('x_2')
figure(3), % class 0 circle, class 1 +, correct green, incorrect red
plot(x4(1,ind400),x4(2,ind400),'og'); hold on,
plot(x4(1,ind410),x4(2,ind410),'or'); hold on,
plot(x4(1,ind401),x4(2,ind401),'+r'); hold on,
plot(x4(1,ind411),x4(2,ind411),'+g'); hold on,
axis equal,
horizontalGrid = linspace(floor(min(x4(1,:))),ceil(max(x4(1,:))),101);
verticalGrid = linspace(floor(min(x4(2,:))),ceil(max(x4(2,:))),91);
[h,v] = meshgrid(horizontalGrid,verticalGrid);
discriminantScoreGridValues = log(evalGaussian([h(:)';v(:)'],mu(:,2),c(:,:,2)))-log(evalGaussian([h(:)';v(:)'],mu(:,1),c(:,:,1)));
minDSGV = min(discriminantScoreGridValues);
maxDSGV = max(discriminantScoreGridValues);
discriminantScoreGrid = reshape(discriminantScoreGridValues,91,101);
figure(3), contour(horizontalGrid,verticalGrid,discriminantScoreGrid,[minDSGV*[0.9,0.6,0.3],0,[0.3,0.6,0.9]*maxDSGV]); % plot equilevel contours of the discriminant function 
% including the contour at level 0 which is the decision boundary
legend('Correct decisions for data from Class 0','Wrong decisions for data from Class 0','Wrong decisions for data from Class 1','Correct decisions for data from Class 1','Equilevel contours of the discriminant function' ,'Location','southeast'), 
title('Data and their classifier decisions versus true labels'),
xlabel('x_1'), ylabel('x_2'), 

%
function g = evalGaussian(x,mu,Sigma)
% Evaluates the Gaussian pdf N(mu,Sigma) at each coumn of X
[n,N] = size(x);
C = ((2*pi)^n * det(Sigma))^(-1/2);
E = -0.5*sum((x-repmat(mu,1,N)).*(inv(Sigma)*(x-repmat(mu,1,N))),1);
g = C*exp(E);
end

