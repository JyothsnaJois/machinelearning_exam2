%%generate and train 10 samples
clc;clear all;close all;n = 2;      % number of feature dimensions
N = 10;
N1=100;
N2=1000;
N3=10000;
mu(:,1)=[-2;0];mu(:,2)=[2;0];
c(:,:,1)=[1 -0.9;-0.9 2];
c(:,:,2)=[2 0.9;0.9 1];
P=[0.9,0.1];
label = (rand(1,N) >= P(1))';
label1 = (rand(1,N1) >= P(1))';
label2 = (rand(1,N2) >= P(1))';
label3 = (rand(1,N3) >= P(1))';

n1 = [length(find(label==0)),length(find(label==1))];
n2 = [length(find(label1==0)),length(find(label1==1))];
n3 = [length(find(label2==0)),length(find(label2==1))];
n4 = [length(find(label3==0)),length(find(label3==1))];
% Draw samples from each class pdf
x = zeros(N,n); 
for i = 0:1
    x(label==i,:) = mvnrnd(mu(:,i+1),c(:,:,i+1),n1(i+1));
end
x1 = zeros(N1,n); 
for i = 0:1
    x1(label1==i,:) = mvnrnd(mu(:,i+1),c(:,:,i+1),n2(i+1));
end
x2 = zeros(N2,n); 
for i = 0:1
    x2(label2==i,:) = mvnrnd(mu(:,i+1),c(:,:,i+1),n3(i+1));
end

%Plot samples with true class labels
figure(1);
plot(x(label==0,1),x(label==0,2),'o',x(label==1,1),x(label==1,2),'+');
legend('Class 0','Class 1'); title('Data and True Class Labels');
xlabel('x_1'); ylabel('x_2'); hold on;
x = [ones(N, 1) x];
start_theta = zeros(n+1, 1);
label=double(label);

% Compute gradient descent to get theta values
% [theta, cost] = grad(x,label,N,start_theta,1,N3);
[theta, cost] = fminsearch(@(t)(cost_func(t, x, label, N)), start_theta);

% Choose points to draw boundary line
px1 = [min(x(:,2))-2,  max(x(:,2))+2];                      
% plot_x2(1,:) = (-1./theta(3)).*(theta(2).*plot_x1 + theta(1));  
px2(2,:) = (-1./theta(3)).*(theta(2).*px1 + theta(1)); % fminsearch

% Plot decision boundary
plot(px1,px2(2,:));  
axis([px1(1), px1(2), min(x(:,3))-2, max(x(:,3))+2]);
legend('Class 0', 'Class 1', 'fminsearch');

% Plot cost function
%% 100
start_theta1 = zeros(n+1, 1);
label1=double(label1);
figure(3);
plot(x1(label1==0,1),x1(label1==0,2),'o',x1(label1==1,1),x1(label1==1,2),'+');
legend('Class 0','Class 1'); title('Data and True Class Labels');
xlabel('x_1'); ylabel('x_2'); hold on;
x1=[ones(N1, 1) x1];
% [stheta, scost] = gradient_descent(x1,N1,label1,initial_theta1,1,1000);
[stheta2, scost2] = fminsearch(@(t)(cost_func(t, x1, label1, N1)), start_theta1);

% Choose points to draw boundary line
spx1 = [min(x1(:,2))-2,  max(x1(:,2))+2];                      
% splot_x2(1,:) = (-1./stheta(3)).*(stheta(2).*splot_x1 + stheta(1));  
spx2(2,:) = (-1./stheta2(3)).*(stheta2(2).*spx1 + stheta2(1)); % fminsearch

% Plot decision boundary
plot(spx1,spx2(2,:));  
axis([spx1(1), spx1(2), min(x1(:,3))-2, max(x1(:,3))+2]);
legend('Class 0', 'Class 1', 'fminsearch');
%% 1000 samples
start_theta2 = zeros(n+1, 1);
label2=double(label2);
figure(5);
plot(x2(label2==0,1),x2(label2==0,2),'o',x2(label2==1,1),x2(label2==1,2),'+');
legend('Class 0','Class 1'); title('Data and True Class Labels');
xlabel('x_1'); ylabel('x_2'); hold on;
x2=[ones(N2, 1) x2];
% [ttheta, tcost] = gradient_descent(x2,N2,label2,initial_theta2,1,1000);
[ttheta2, tcost2] = fminsearch(@(t)(cost_func(t, x2, label2, N2)), start_theta2);

% Choose points to draw boundary line
tpx1 = [min(x2(:,2))-2,  max(x2(:,2))+2];                      
% tplot_x2(1,:) = (-1./ttheta(3)).*(ttheta(2).*tplot_x1 + ttheta(1));  
tpx2(2,:) = (-1./ttheta2(3)).*(ttheta2(2).*tpx1 + ttheta2(1)); % fminsearch

% Plot decision boundary
plot(tpx1, tpx2(2,:));  
axis([tpx1(1), tpx1(2), min(x2(:,3))-2, max(x2(:,3))+2]);
legend('Class 0', 'Class 1', 'fminsearch');

%% test data 10000 samples
x3 = zeros(N3,n); 
for i = 0:1
    x3(label3==i,:) = mvnrnd(mu(:,i+1),c(:,:,i+1),n4(i+1));
end
%% test
coeff(2,:) = polyfit([tpx1(1), tpx1(2)], [tpx2(2,1), tpx2(2,2)], 1);
% coeff(1,:) = polyfit([splot_x1(1), splot_x1(2)], [splot_x2(1,1), splot_x2(1,2)], 1);
for i = 2
    if coeff(i,1) >= 0
        decision(:,i) = (coeff(i,1).*x3(:,1) + coeff(i,2)) < x3(:,2);
    else
        decision(:,i)= (coeff(i,1).*x3(:,1) + coeff(i,2)) > x3(:,2);
    end
end

% error1 = plot_test_data(decision(:,1), label3, n4, P, 3, x3, splot_x1,splot_x2(1,:));
% title('Test Data Classification (from scratch)');
% fprintf('Total error (classifier from scratch): %.2f%%\n',error1);

error2 = plot_test_data(decision(:,2), label3, n4, P, 4, x3, tpx1, tpx2(2,:));
title('Test Data Classification (using fminsearch)');
fprintf('Total error (classifier using fminsearch): %.2f%%\n',100-error2);


function cost = cost_func(theta, x, label,N)
    h = 1 ./ (1 + exp(-x*theta));	% Sigmoid function
    cost = (-1/N)*((sum(label' * log(h)))+(sum((1-label)' * log(1-h))));
end
function error = plot_test_data(decision, label, Nc, p, fig, x, plot_x1, plot_x2)
    ind00 = find(decision==0 & label==0); % true negative
    ind10 = find(decision==1 & label==0); p10 = length(ind10)/Nc(1); % false positive
    ind01 = find(decision==0 & label==1); p01 = length(ind01)/Nc(2); % false negative
    ind11 = find(decision==1 & label==1); % true positive
    error = (p10*p(1) + p01*p(2))*100;

    % Plot decisions and decision boundary
    figure(fig);
    plot(x(ind00,1),x(ind00,2),'or'); hold on,
    plot(x(ind10,1),x(ind10,2),'og'); hold on,
    plot(x(ind01,1),x(ind01,2),'+g'); hold on,
    plot(x(ind11,1),x(ind11,2),'+r'); hold on,
    plot(plot_x1, plot_x2);
    axis([plot_x1(1), plot_x1(2), min(x(:,2))-2, max(x(:,2))+2])
%     legend('Class 0 Correct Decisions','Class 0 Wrong Decisions','Class 1 Wrong Decisions','Class 1 Correct Decisions','Classifier');
end