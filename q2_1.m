clc;close all;clear all;
N=10;%samples in x
n=3;%power of polynomial
Nsamples=100;%no of experiments
Vsigma=0.5;%sigma for Noise
a1=-1;b1=1;
s1=[-1:0.65:1];
wtrue = a1+(b1-a1).*rand(length(s1),1); %true parameters
a=-1;b=1;
s=[-1:0.22:1];
x = a + (b-a).*rand(length(s),1);%x uniform data
X = zeros(n+1,N);%store powers of x in X
Ngamma=100;
gamma=10.^linspace(-10,10,Ngamma);
error=zeros(Nsamples,Ngamma);
%analysis for 100 values
for i=1:Ngamma
    G=gamma(i)
    %for each gamma value find MAP estimates
    for j=1:Nsamples
        a=-1;b=1;
        s=[-1:0.22:1];
        for l=1:N
        x = a + (b-a).*rand(length(s),1);%x uniform data
        X = zeros(n+1,N)
        v = Vsigma*randn(1,N);
        X(1,:) = ones(1,N);
        for k = 1:n
            X(k+1,:) = x.^k; %Saving powers of x in each Row of X
        end
       %evaluating y from the given quadratic relation
        end
        y=wtrue'*X+v; 
        wmap = inv(X*X'+(Vsigma/G)^2*eye(n+1))*(X*y');
        error(j,i)=norm(wmap-wtrue,2);
%         Y = prctile(error,[0,5,25,50,75,95,100],1);
%         Y1=mean(error);
%         figure(1), loglog(gamma,Y),
%         minimum=min(error);
%         maximum=max(error);
%         xlabel('Gamma'), ylabel('Parameter Squared Error Percentiles'),
%         title('Percentiles [0,5,25,50,75,95,100]% are shown...'),
    end
end
Y = prctile(error,[25,75],1);
Y1=mean(error);
med=median(error);
figure(1), loglog(gamma,Y,'b'),hold on
minimum=min(error);
maximum=max(error);
mean=mean(error);
loglog(gamma,minimum,'r')
loglog(gamma,maximum,'k')
loglog(gamma,med,'c')
% loglog(gamma,Y1,'y')
xlabel('Gamma'), ylabel('squared error'),
legend('25%','75%','minimum','maximum','median');

    