clc;close all; clear all;
B=10;regWeight = 1e-10;
N=10;N1=100;N2=1000;k=10;
p=[0.3,0.2,0.3,0.2];
mu(:,1)=[1;2];mu(:,2)=[2;2];
mu(:,3)=[1;2];mu(:,4)=[2;2];
sigma(:,:,1)=[0.2 0;0 0.2];sigma(:,:,2)=[1 -4;-4 0.5];
sigma(:,:,3)=[1 0;0 0.2];sigma(:,:,4)=[0.2 0;0 1];
d=size(mu,1);
[D,M]=size(mu);
u = rand(1,N2); xtrue = zeros(d,N2); labels = zeros(1,N2);
for m = 1:length(p)
    cum_alpha = [0,cumsum(p)];
    ind = find(cum_alpha(m)<u & u<=cum_alpha(m+1)); 
    xtrue(:,ind) = randGaussian(length(ind),mu(:,m),sigma(:,:,m));
end
xtrue1=real(xtrue);
xtrue2=xtrue1
figure
y = [zeros(1000,1);ones(1000,1)];
h = scatter(xtrue(:,1),xtrue(:,2));
hold on
% 
for l=1:6
    NegativeLogLikelihood=zeros(6,1);
    eva=zeros(6,1);
    gm=cell(6,1);
    options = statset('MaxIter',100);
    for j=1:k
      data_validate=xtrue(:,j); 
      indices=j;
      xtrue1(:,j:j)=[];
      train=[xtrue1];
      xtrue1=xtrue2;
      
    end
    gm{l}=fitgmdist(train',l,'Covtype','diagonal','Options',options);
    NegativeLogLikelihood(l)=gm{l}.NegativeLogLikelihood
    eva(l,k)=max(NegativeLogLikelihood);
end

    


function x = randGaussian(N,mu,Sigma)
% Generates N samples from a Gaussian pdf with mean mu covariance Sigma
n = length(mu);
z =  randn(n,N);
A = Sigma^(1/2);
x = A*z + repmat(mu,1,N);
end

